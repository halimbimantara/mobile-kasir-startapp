<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;

/**
 * Todo
 * Member list  
 */
class Api extends BaseController
{
	function __construct()
	{
		$this->connect = \Config\Database::connect();
	}

	public function cekLogin()
	{
		$username = $this->session->get('username');
		if (!$username) {
			return redirect()->to(base_url('login'));
		}
    }

    public function index(){
        
    }

    public function add(){
        
    }

    public function processSaveMember(){
        
    }
}