<?php namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use App\Models\Auth_model;

class Welcome extends BaseController
{
	protected $session;
	function __construct()
	{
		helper('form','url');
		$this->validation = \Config\Services::validation();
		$this->session = \Config\Services::session();
		$this->connect = \Config\Database::connect();
	}

	public function index()
	{	
		$data = array('title'=>"Pos Umkm",'username' => "" ,"link_download"=>"https://drive.google.com/file/d/1BQga4xMFlgz6-vJ_o6onko4n_H0zKd8L/view?usp=sharing");
		// $data = array('title'=>"Pos Umkm",'username' => "" ,"link_download"=>"https://github.com/halimbimantara/pos_umk/archive/master.zip");
		return view('main_welcome',$data);
	}


	public function dokumentasi(){
		return view('documentation/index');
	}

	public function policy(){
		return view('documentation/policy');
	}
}
