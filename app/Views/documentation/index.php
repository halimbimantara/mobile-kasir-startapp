<!DOCTYPE html>
<html lang="en">

<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">
    <meta name="description" content="Aplikasi Kasir">
    <meta name="keywords" content="Pos,Mobile Kasir,Solusi Kasir">
    <meta name="author" content="ColorLib">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ========== Title ========== -->
    <title> Kakao (Kasir )</title>
    <!-- ========== Favicon Ico ========== -->
    <!--<link rel="icon" href="fav.ico">-->
    <!-- ========== STYLESHEETS ========== -->
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url("resources/assets/css/bootstrap.min.css") ?>" rel="stylesheet">
    <!-- Fonts Icon CSS -->
    <link href="<?php echo base_url("resources/assets/css/font-awesome.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/et-line.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/ionicons.min.css") ?>" rel="stylesheet">
    <!-- Carousel CSS -->
    <link href="<?php echo base_url("resources/assets/css/owl.carousel.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("resources/assets/css/owl.theme.default.min.css") ?>" rel="stylesheet">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url("resources/assets/css/animate.min.css") ?>">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url("resources/assets/css/main.css") ?>" rel="stylesheet">
</head>

<body>
    <div class="loader">
        <div class="loader-outter"></div>
        <div class="loader-inner"></div>
    </div>

    <!--header start here -->
    <header class="header navbar fixed-top navbar-expand-md">
        <div class="container">
            <a class="navbar-brand logo" href="#">
                <img src="<?php echo base_url("resources/assets/img/logomobi.png") ?>" alt="Evento" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headernav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="lnr lnr-text-align-right"></span>
            </button>
            <div class="collapse navbar-collapse flex-sm-row-reverse" id="headernav">
                <ul class=" nav navbar-nav menu">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url("/") ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="<?php echo base_url("dokumentasi") ?>">Installasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url("register") ?>">Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="<?php echo base_url("login") ?>">Login</a>
                    </li>

                    <li class="search_btn">
                        <a href="#">
                            <i class="ion-ios-search"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!--header end here-->

    <!--events section -->
    <section class="pt100 pb100">
        <div class="container">

            <div class="event_box">
                <!-- <img src="assets/img/events/event1.png" alt="event"> -->
                <div class="event_info">
                    <div class="event_title">
                        Panduan Instalasi
                    </div>
                  
            </div>



        </div>
    </section>
    <!--event section end -->
    <!--footer start -->
    <footer>
        <div class="container">
            <div class="row justify-content-center">





            </div>
        </div>
    </footer>
    <div class="copyright_footer">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-12">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | Mobile Kasir</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>

            </div>
        </div>
    </div>
    <!--footer end -->

    <!-- jquery -->
    <script src="<?php echo base_url("resources/assets/js/jquery.min.js") ?>"></script>
    <!-- bootstrap -->
    <script src="<?php echo base_url("resources/assets/js/popper.js") ?>"></script>
    <script src="<?php echo base_url("resources/assets/js/bootstrap.min.js") ?>"></script>
    <script src="<?php echo base_url("resources/assets/js/waypoints.min.js") ?>"></script>
    <!--slick carousel -->
    <script src="<?php echo base_url("resources/assets/js/owl.carousel.min.js") ?>"></script>
    <!--parallax -->
    <script src="<?php echo base_url("resources/assets/js/parallax.min.js") ?>"></script>
    <!--Counter up -->
    <script src="<?php echo base_url("resources/assets/js/jquery.counterup.min.js") ?>"></script>
    <!--Counter down -->
    <script src="<?php echo base_url("resources/assets/js/jquery.countdown.min.js") ?>"></script>
    <!-- WOW JS -->
    <script src="<?php echo base_url("resources/assets/js/wow.min.js") ?>"></script>
    <!-- Custom js -->
    <script src="<?php echo base_url("resources/assets/js/main.js") ?>"></script>
</body>

</html>