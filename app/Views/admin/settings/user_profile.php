<?= $this->extend('default_layout') ?>
<?= $this->section('content') ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="<?= ( $users[0]->image !=null ? base_url()."/resources/uploads/logo/".$users[0]->image : base_url()."/resources/dist/img/avatar.png" ) ?>" alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center"><?php echo $users[0]->first_name; ?></h3>

                    <!-- <p class="text-muted text-center">Jabatan</p> -->



                    <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tentang saya</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <strong><i class="fas fa-book mr-1"></i> Jabatan</strong>

                    <p class="text-muted">
                    <?php echo $role == "" ?"Member":$role; ?>
                    </p>

                    <hr>

                    <hr>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <!-- <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Transaksi</a></li> -->
                        <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Settings</a></li>
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                       

                        <div class="tab-pane active" id="settings">
                            <?= form_open_multipart(base_url('profile/process')); ?>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $users[0]->first_name; ?>" class="form-control" name="first_name" id="inputName" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $users[0]->email; ?>" class="form-control" name="email" id="inputName" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Username</label>
                                    <div class="col-sm-10">
                                        <?php echo $users[0]->username; ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail" class="col-sm-2 col-form-label">No telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="phone" value="<?php echo $users[0]->phone; ?>" class="form-control" id="inputEmail" placeholder="No Tlpn">
                                    </div>
                                </div>
                            
                                <div class="form-group row">
                                    <label for="inputSkills" class="col-sm-2 col-form-label">Password Baru</label>
                                    <div class="col-sm-10">
                                        <input type="password" name="password" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputSkills" class="col-sm-2 col-form-label">Upload Photo</label>
                                    <div class="col-sm-10">
                                    <div class="custom-file">
                                        <input type="file" name="file_upload" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            <?= form_close() ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->

<?= $this->endSection() ?>

<? $this->section('jscript'); ?>

<!-- bs-custom-file-input -->
<script src="<?= base_url("resources/plugins/bs-custom-file-input/bs-custom-file-input.min.js")?>"></script>

<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>

<?= $this->endSection(); ?>